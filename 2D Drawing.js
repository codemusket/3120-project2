/**
 * @author Jialei Li, K.R. Subrmanian, Zachary Wartell
 * 
 * 
 */


/*****
 * 
 * GLOBALS
 * 
 *****/

// 'draw_mode' are names of the different user interaction modes.
// \todo Student Note: others are probably needed...
var draw_mode = {DrawLines: 0, DrawTriangles: 1, ClearScreen: 2, None: 3, DrawQuads : 4};

// 'curr_draw_mode' tracks the active user interaction mode
var curr_draw_mode = draw_mode.DrawLines;

// GL array buffers for points, lines, and triangles
// \todo Student Note: need similar buffers for other draw modes...
var vBuffer_Pnt, vBuffer_Line;
var vBuffer_Tri, vBuffer_Quad;

// Array's storing 2D vertex coordinates of points, lines, triangles, etc.
// Each array element is an array of size 2 storing the x,y coordinate.
// \todo Student Note: need similar arrays for other draw modes...
var points = [], line_verts = [], tri_verts = [], quad_verts = [];

// count number of points clicked for new line
var num_pts_line = 0;

// \todo need similar counters for other draw modes...
var num_pts_tri = 0;
var num_pts_quad = 0;

var line_threshold = .015;
var tri_threshold = 1.1;

// Colors

var points_r = 1;
var points_g = 1;
var points_b = 1;

var slider_red  = 0;
var slider_green = 1;
var slider_blue = 0;

var line_r = slider_red;
var line_g = slider_green;
var line_b = slider_blue;

var tri_r = slider_red;
var tri_g = slider_green;
var tri_b = slider_blue;

var quad_r = slider_red;
var quad_g = slider_green;
var quad_b = slider_blue;


var last_x = 0;
var last_y = 0;


// List of any selected shapes
// Stored in a pair format: 
//  [type, data]
// line: [0, [ [0,1], [1,1] ]              ]
// tri:  [1, [ [0,1], [0,2], [1,1]         ]
// quad: [2, [ [0,1], [0,2], [1,1], [3,3]  ]
/* Ex:
    var selected = [ 
                    [0, [[0,0], [1,1]] ] 
                   ] ;
*/
var selected = [];
var selected_idx = 0;






/*****
 * 
 * MAIN
 * 
 *****/
function main() {
    
    //math2d_test();
    
    /**
     **      Initialize WebGL Components
     **/
    
    // Retrieve <canvas> element
    var canvas = document.getElementById('webgl');

    // Get the rendering context for WebGL
    var gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('Failed to get the rendering context for WebGL');
        return;
    }

    // Initialize shaders
    if (!initShadersFromID(gl, "vertex-shader", "fragment-shader")) {
        console.log('Failed to intialize shaders.');
        return;
    }

    // create GL buffer objects
    vBuffer_Pnt = gl.createBuffer();
    if (!vBuffer_Pnt) {
        console.log('Failed to create the buffer object');
        return -1;
    }

    vBuffer_Line = gl.createBuffer();
    if (!vBuffer_Line) {
        console.log('Failed to create the buffer object');
        return -1;
    }

    // create GL buffer objects
    vBuffer_Tri = gl.createBuffer();
    if (!vBuffer_Tri) {
        console.log('Failed to create the buffer object');
        return -1;
    }

    vBuffer_Quad = gl.createBuffer();
    if (!vBuffer_Quad) {
        console.log('Failed to create the buffer object');
        return -1;
    }

    var skeleton=true;
    if(skeleton)
    {
        document.getElementById("App_Title").innerHTML += "-Skeleton";
    }

    // \todo create buffers for triangles and quads...

    // Specify the color for clearing <canvas>
    gl.clearColor(0, 0, 0, 1);

    // Clear <canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);

    // get GL shader variable locations
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('Failed to get the storage location of a_Position');
        return -1;
    }

    var u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
    if (!u_FragColor) {
        console.log('Failed to get the storage location of u_FragColor');
        return;
    }

    /**
     **      Set Event Handlers
     **
     **  Student Note: the WebGL book uses an older syntax. The newer syntax, explicitly calling addEventListener, is preferred.
     **  See https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
     **/
    // set event handlers buttons
    document.getElementById("LineButton").addEventListener(
            "click",
            function () {
                curr_draw_mode = draw_mode.DrawLines;
            });

    document.getElementById("TriangleButton").addEventListener(
            "click",
            function () {
                curr_draw_mode = draw_mode.DrawTriangles;
            });    
    
    document.getElementById("ClearScreenButton").addEventListener(
            "click",
            function () {
                curr_draw_mode = draw_mode.ClearScreen;
                // clear the vertex arrays
                while (points.length > 0)
                    points.pop();
                while (line_verts.length > 0)
                    line_verts.pop();
                while (tri_verts.length > 0)
                    tri_verts.pop();
                while (quad_verts.length > 0)
                    quad_verts.pop();

                gl.clear(gl.COLOR_BUFFER_BIT);
                
                curr_draw_mode = draw_mode.DrawLines;
            });
            
    //\todo add event handlers for other buttons as required....            
    document.getElementById("QuadButton").addEventListener(
            "click",
            function () {
                curr_draw_mode = draw_mode.DrawQuads;
            });    

    document.getElementById("DeleteButton").addEventListener(
            "click",
            function () {
                deleteSelected();
                drawObjects(gl,a_Position, u_FragColor);
            });    


    // set event handlers for color sliders
    /* \todo right now these just output to the console, code needs to be modified... */

    document.getElementById("RedRange").addEventListener(
            "input",
            function () {
                //console.log("RedRange:" + document.getElementById("RedRange").value);
                
                // Update global slider color
                slider_red = document.getElementById("RedRange").value;

                //change the selected type color
                changeSelectedTypeColor(slider_red, slider_green, slider_blue);

                // Update Screen
                drawObjects(gl,a_Position, u_FragColor);
            });
    document.getElementById("GreenRange").addEventListener(
            "input",
            function () {
                //console.log("GreenRange:" + document.getElementById("GreenRange").value);
                
                slider_green = document.getElementById("GreenRange").value;

                changeSelectedTypeColor(slider_red, slider_green, slider_blue);

                drawObjects(gl,a_Position, u_FragColor);
            });
    document.getElementById("BlueRange").addEventListener(
            "input",
            function () {
                //console.log("BlueRange:" + document.getElementById("BlueRange").value);
                
                slider_blue = document.getElementById("BlueRange").value;

                changeSelectedTypeColor(slider_red, slider_green, slider_blue);

                drawObjects(gl,a_Position, u_FragColor);
            });                        
            
    // init sliders 
    // \todo this code needs to be modified ... - done

    setSliders(slider_red, slider_green, slider_blue);

    // Register function (event handler) to be called on a mouse press
    canvas.addEventListener(
            "mousedown",
            function (ev) {
                handleMouseDown(ev, gl, canvas, a_Position, u_FragColor);
                });

    // Override context menu
    canvas.oncontextmenu = function (e) {
        e.preventDefault();
    };
}

/*****
 * 
 * FUNCTIONS
 * 
 *****/

/*
 * Handle mouse button press event.
 * 
 * @param {MouseEvent} ev - event that triggered event handler
 * @param {Object} gl - gl context
 * @param {HTMLCanvasElement} canvas - canvas 
 * @param {Number} a_Position - GLSL (attribute) vertex location
 * @param {Number} u_FragColor - GLSL (uniform) color
 * @returns {undefined}
 */

function pointsListEqual( pointsA, pointsB ) {

    if( pointsA.length != pointsB.length )
        return false;

    for( var i = 0; i < pointsA.length; i++ ) 
        if( pointsA[i] != pointsB[i])
            return false;
    
    return true;
}


function deleteSelected() {

    // a = [1,2,3,4]
    // a = a.slice(0,2).concat(a.slice(3,4))    // [1,2,3]

    if( !selected.length ) {
        console.log("Nothing selected! No deleting.");
        return;
    }

    var type = getSelectedType();

    // if line is selected
    if( type == 0 ) {

        // get index of line
        // find the two points
        //      line_verts
        // delete them
        // recopy line_vert array to not have undefined areas

        // Scan each line in line_verts
        var new_line_verts = []
        for( i = 0; i < line_verts.length && line_verts[i+1]; i+= 2 ) 
        {

            //line = packLineSelection(line_vert[i], line_vert[i+1]);
            var line = [ line_verts[i], line_verts[i+1] ];

            // work on this get selected points
            // one is a line, one is a pack
            if( lineEqual( line, getSelectedLine())  ) {
                console.log("Found the line! Delete it!");
            }
            else {
                new_line_verts.push(line_verts[i]);
                new_line_verts.push(line_verts[i+1]);
            }

        }// end loop lines

        line_verts = new_line_verts;
    }
    // If Triangle Selected
    else if( type == 1 ) {

        var new_tri_verts = [];
        for( var i = 0; i < tri_verts.length && tri_verts[i+1]
                                             && tri_verts[i+2]; i += 3 ) 
        {

            var tri_pts = [ tri_verts[i], tri_verts[i+1], tri_verts[i+2] ];
            var selected_tri_pts = [ getSelectedPoint(0), getSelectedPoint(1) , 
                                     getSelectedPoint(2) ];
            
            if( pointsListEqual(tri_pts, selected_tri_pts) )  {
                console.log("Found the triangle! Delete it!");
            }
            else {
                new_tri_verts.push( tri_verts[i],
                                    tri_verts[i+1],
                                    tri_verts[i+2]
                                    );
            }

        }// end loop thought triangles

        tri_verts = new_tri_verts;

    }
    // If quad selected, delete the quad!
    else if( type == 2 ) {
        
        // Loop each quad to see if it matches selected quad
        var new_quad_verts = [];
        for( var i = 0; i < quad_verts.length && 
                              quad_verts[i+1] &&
                              quad_verts[i+2] &&
                              quad_verts[i+3]    ; i += 4 ) 
        {

            var quad_pts = [ quad_verts[i], quad_verts[i+1],
                             quad_verts[i+2], quad_verts[i+3] ];

            var selected_quad_pts = [ getSelectedPoint(0), getSelectedPoint(1),
                                      getSelectedPoint(2), getSelectedPoint(3) ];

            if( pointsListEqual( quad_pts, selected_quad_pts) ) {
                // Do nothing with it, leave it alone!
                console.log("Deleting the quad!");
            }
            else {
                // Don't delete this one, so add it to the list
                new_quad_verts.push( quad_verts[i],
                                      quad_verts[i+1],
                                      quad_verts[i+2],
                                      quad_verts[i+3]
                                      );
            }
        }   // done looping 
        quad_verts = new_quad_verts;
    } // done with quads

    clearSelection();
}

// Takes in standard colors 0.0 to 1.0, converts to appropriate values
function setSliders(r, g, b) {
    document.getElementById("RedRange").value   = r * 100;
    document.getElementById("GreenRange").value = g * 100;
    document.getElementById("BlueRange").value  = b * 100;
}
function setSlidersToSelectedColor() {

    var type = getSelectedType();
    var r, g, b;

    if( type == 0 )
    {
        r = line_r;
        g = line_g;
        b = line_b;
    }
    else if( type == 1 ) {
        r = tri_r;
        g = tri_g;
        b = tri_b;
    }
    else if( type == 2 ) {
        r = quad_r;
        g = quad_g;
        b = quad_b;
    }

    setSliders(r,g,b);
}

function changeSelectedTypeColor( r, g, b ) {
    changeColor( getSelectedType(), r, g, b );
}
// Take in value 0 - 100
function changeColor(type, r, g, b) {

    r = r/100;
    g = g/100;
    b = b/100;

    switch( type ) {
        case -1:
            points_r = r;
            points_g = g;
            points_b = b;
            break;
        case 0:
            line_r = r;
            line_g = g;
            line_b = b;
            break;
        case 1 :
            tri_r = r;
            tri_g = g;
            tri_b = b;
            break;
        case 2:
            quad_r = r;
            quad_g = g;
            quad_b = b;
            break;
    }



}


function packSelection(type, points) {
    return [type, points]
}

function packLineSelection(p0, p1) {
    return packSelection(0, [p0, p1]);
}
function packTriSelection(p0, p1, p2) {
    return packSelection(1, [p0, p1, p2]);
}
function getSelectedLine(idx = selected_idx) {
    return selected[idx][1];
}

function getSelectedPoint( ith_point, ith_selection = selected_idx, arr = selected ) {
    return arr[ith_selection][1][ith_point];
}

function pointsEqual( p0, p1 ) {
    if( p0[0] == p1[0] && p0[1] == p1[1] )
        return true;
    return false;
}
function lineEqual( line0, line1 ) {
    // line ~ [ [0,0], [1,1] ]

    if( pointsEqual( line0[0], line1[0]) && 
        pointsEqual( line0[1], line1[1]) )
        return true;
        
    if( pointsEqual( line0[0], line1[1]) && 
        pointsEqual( line0[1], line1[0])  )
        return true;

    return false;
}
function getSelectedType(idx = selected_idx, arr = selected) {
    return arr[idx][0];
}
/*
// Turns out not needed!
function checkSelectedList(test_selection) {
    

    // Scan through each selection
    for( i = 0; i < selected.length; i++ ) {

        var type = getSelectedType(i);

        if( type != test_selection[0] ) {
            console.log("Wrong type.. skip to next in selected");
            continue;
        }
        // get the coordinates
        selected_points = selected[i][1].slice();
        test_points = test_selection[1].slice();

        switch( type ) {
            
            // Works for comparing lines
            case 0:
                //  shape_verts[idx][coords_field=1][coord_idx]
                //  check point 1

                var s = selected_points;
                var t = test_points;

                if( lineEqual( s, t )  )
                    return true;
                //else
                  //  console.log(v[0] + "," + v[1] + " != " + test_selection[0] + "," + test_selection[1])
            case 1:
                break;
            case 2:
                break;
        }
    
        
     }
     return false;
}
*/
function handleSelection(x, y, gl, canvas, a_Position, u_FragColor) {

     // [SELECTION] 

        // If mouse hasn't moved && objects in selected list
        //      selected index ++
        
        // Else 
        //      selected index = 0
        //      reset selected list
        //      Find close shape(s) to point
        //          check lines 
        //          check triangles
        //          check quads
        //              add all items that pass threshold to list

        // [Selected Object Indication]

        // Add handles to the item in list, that has the counter
        // Set color handles to object type color (line_color, tri_color, quad_color)

        // [DELETE] 
        // On 'delete' button click
        //  if selected object,
        //      remove vertices from object type list

        // [COLOR CHANGE]
        // On change of color handles,
        //      change color of selected object type color
          

        //console.log("X: " + x);
        //console.log("last_x: " + last_x + "\n");

        // If mouse hasn't moved && there is more than 1 selected object in list
        if( last_x == x  && last_y == y && selected.length > 1) {

            // cycle currently selected
            if( selected_idx >= selected.length - 1 )
                selected_idx = 0;
            else
                selected_idx++;

            console.log("Mouse hasn't moved. Selected idx: " + selected_idx);

        }
        else // make a new selection
        {
            // clear out selection since new mouse click, and has moved
            clearSelection();

            // Find all shapes within threshold

            var mouseClick = new Vec2([x,y]);

            ///
            // Search all lines
            ///

            if( line_verts.length > 1 ) {

                for( var i = 0; i < line_verts.length && line_verts[i+1]; i += 2 ) {

                    var p1 = new Vec2(line_verts[i]);
                    var p2 = new Vec2(line_verts[i+1]);

                    if( !line_verts[i] || !line_verts[i+1])
                        console.log("Bad input!");

                    console.log(line_verts.length);
                    console.log("i " + i);
                    console.log("p1 " + p1.x + ", "+ p1.y + " .. p2: " + p2.x + ", " + p2.y);
                    pDist = pointLineDist(p1, p2, mouseClick);

                    // deconvert points back to simple vectors
                    p1 = line_verts[i];
                    p2 = line_verts[i+1];

                    //console.log("pDist: "+ pDist.x + ", " + pDist.y + ", Mag: "+ pDist.mag() );

                    if( pDist.mag() <= line_threshold ) {

                        console.log("Click hit the line (or close enough)!");
                        selected.push( packLineSelection(p1, p2)  );
                        
                        //console.log(selected);
                    }
                    //console.log("i:" + i)
                    console.log(p1.x + ", " + p1.y);
                    console.log(p2.x + ", " + p2.y);
                

                } // Done seraching lines for hit 
            }
            /// 
            // Search for triangles!
            ///
            if( tri_verts.length  > 2 ) {

                for( var i = 0; i < tri_verts.length
                                    && tri_verts[i] 
                                    && tri_verts[i + 1] 
                                    && tri_verts[i + 2]; 

                                i += 3) {

                    var a = new Vec2(tri_verts[i]);
                    var b = new Vec2(tri_verts[i+1]);
                    var c = new Vec2(tri_verts[i+2]);
                  

                    var barycentric_coords = barycentric(
                                                a, 
                                                b, 
                                                c,
                                                mouseClick);

                    var test =  barycentric_coords[0] + 
                                barycentric_coords[1] +
                                barycentric_coords[2];


                    //console.log(barycentric_coords);
                    //console.log(test);

                    if( test <= tri_threshold ) {
                        console.log("hit triangle!");
                        console.log()
                        // Show handles 
                        selected.push(
                                    packTriSelection( tri_verts[i],
                                                      tri_verts[i+1],
                                                      tri_verts[i+2])
                                  );
                    }   // end hit

                }   // end loop each tri_vert(s)
            }   // done searching for triangles

            //
            // Check for Quads! 
            // 

            if( quad_verts.length > 3 ) {

                for( var i = 0; i < quad_verts.length
                                    && quad_verts[i] 
                                    && quad_verts[i + 1] 
                                    && quad_verts[i + 2]
                                    && quad_verts[i + 3]; 

                                i += 4) {

                    var a = new Vec2(quad_verts[i]);
                    var b = new Vec2(quad_verts[i+1]);
                    var c = new Vec2(quad_verts[i+2]);
                    var d = new Vec2(quad_verts[i+3]);
                  

                    var barycentric_coords_1 = barycentric(
                                                a, 
                                                b, 
                                                c,
                                                mouseClick);

                    var barycentric_coords_2 = barycentric(
                                                b, 
                                                c, 
                                                d,
                                                mouseClick);

                    var test_1 =    barycentric_coords_1[0] + 
                                    barycentric_coords_1[1] +
                                    barycentric_coords_1[2];

                    var test_2 =    barycentric_coords_2[0] + 
                                    barycentric_coords_2[1] +
                                    barycentric_coords_2[2];


                    //console.log(barycentric_coords);
                    //console.log(test);

                    if( test_1 <= tri_threshold || test_2 <= tri_threshold ) {

                        console.log("hit quad!");
                        console.log("test1: " + test_1 + " test2: " + test_2);

                        // Show handles 
                        /*
                        same as..
                        selected.push(
                                       [3, [ 
                                            quad_verts[i],
                                            quad_verts[i+1],
                                            quad_verts[i+2],
                                            quad_verts[i+3]
                                            ]
                                        ]);
                        */
                        // Push quad to selection
                        // type 2 = quads
                        selected.push(packSelection(2, [ 
                                                        quad_verts[i],
                                                        quad_verts[i+1],
                                                        quad_verts[i+2],
                                                        quad_verts[i+3]
                                                        ]
                                                           ));
                    }   // end hit

                }   // end loop each tri_vert(s)
            }


        
        }

        // Selection Indication

        // for each point in selected shape
        if( selected.length )
            for( var a = 0; a < selected[selected_idx][1].length; a++ )
            {
                points.push(selected[selected_idx][1][a]);                   
            }


        if( selected.length )
            setSlidersToSelectedColor();

        // Draw selection points
        changeColor(-1, 100, 100, 0);

        // Redraw Screen
        drawObjects(gl,a_Position, u_FragColor);

        // Clear points for next frame
        points = [];
        
        // Change point color back for next frame
        changeColor(-1, 100, 100, 100);
        
        last_x = x;
        last_y = y;

}   // /handleSelection(x,y, gl, canvas, a_Position, u_FragColor)

function clearSelection() {
    selected_idx = 0;
    selected = [];
    console.log("Selection cleared");
}

function handleMouseDown(ev, gl, canvas, a_Position, u_FragColor) {
    var x = ev.clientX; // x coordinate of a mouse pointer
    var y = ev.clientY; // y coordinate of a mouse pointer
    var rect = ev.target.getBoundingClientRect();
    
    // Student Note: 'ev' is a MouseEvent (see https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent)
    
    // convert from canvas mouse coordinates to GL normalized device coordinates
    x = ((x - rect.left) - canvas.width / 2) / (canvas.width / 2);
    y = (canvas.height / 2 - (y - rect.top)) / (canvas.height / 2);

    // Detect Right Mouse Click
    if( ev.which == 3 ) {
        
        handleSelection(x, y, gl, canvas, a_Position, u_FragColor);
        return;
    }

    // There was a regular click, so no selection
    clearSelection();


    if (curr_draw_mode !== draw_mode.None) {
        // add clicked point to 'points'
        points.push([x, y]);
    }

    // perform active drawing operation
    switch (curr_draw_mode) {
        case draw_mode.DrawLines:
            // in line drawing mode, so draw lines
            if (num_pts_line < 1) {			
                // gathering points of new line segment, so collect points
                line_verts.push([x, y]);
                num_pts_line++;
            }
            else {						
                // got final point of new line, so update the primitive arrays
                line_verts.push([x, y]);
                num_pts_line = 0;
                points.length = 0;
            }
            break;
        case draw_mode.DrawTriangles:

            // in tri drawing mode
            if( num_pts_tri < 2 ) {
                // collect points for tri
                tri_verts.push([x, y]);
                num_pts_tri++;
            }
            else {
                // got final tri point, so update array
                tri_verts.push([x, y]);
                num_pts_tri = 0;
                points.length = 0;
            }
            break;
        case draw_mode.DrawQuads:


            if( num_pts_quad < 3 ) {
                // collect points for quad
                quad_verts.push([x, y]);
                num_pts_quad++;
            }
            else {
                // got final quad point, update array
                quad_verts.push([x, y]);
                num_pts_quad = 0;
                points.length = 0;
            }
            break;
    }
    

    drawObjects(gl,a_Position, u_FragColor);
}

/*
 * Draw all objects
 * @param {Object} gl - WebGL context
 * @param {Number} a_Position - position attribute variable
 * @param {Number} u_FragColor - color uniform variable
 * @returns {undefined}
 */
function drawObjects(gl, a_Position, u_FragColor) {

    // Clear <canvas>
    gl.clear(gl.COLOR_BUFFER_BIT);

    // draw lines
    if (line_verts.length) {	

        // enable the line vertex
        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer_Line);
        // set vertex data into buffer (inefficient)
        gl.bufferData(gl.ARRAY_BUFFER, flatten(line_verts), gl.STATIC_DRAW);
        // share location with shader
        gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(a_Position);

        gl.uniform4f(u_FragColor, line_r, line_g, line_b, 1.0);

        // draw the lines
        gl.drawArrays(gl.LINES, 0, line_verts.length );
    }

   // \todo draw triangles
   
   if( tri_verts.length > 2 ) {

        // enable the line vertex
        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer_Tri);
        // set vertex data into buffer (inefficient)
        gl.bufferData(gl.ARRAY_BUFFER, flatten(tri_verts), gl.STATIC_DRAW);
        // share location with shader
        gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(a_Position);

        gl.uniform4f(u_FragColor, tri_r, tri_g, tri_b, 1.0);

        gl.drawArrays(gl.TRIANGLES, 0, tri_verts.length);

   }

   // \todo draw quads
    
    if( quad_verts.length > 3 ) {

        // enable the line vertex
        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer_Quad);
        // set vertex data into buffer (inefficient)
        gl.bufferData(gl.ARRAY_BUFFER, flatten(quad_verts), gl.STATIC_DRAW);
        // share location with shader
        gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(a_Position);

        gl.uniform4f(u_FragColor, quad_r, quad_g, quad_b, 1.0);


        // For each group of four points
        //  And ensure the next four points are there for the last quad to prevent attempting
        //  to draw a quad with less than four points
        for( var i = 0; i < quad_verts.length && (i + 4) <= quad_verts.length; i += 4 ) 
            gl.drawArrays(gl.TRIANGLE_STRIP, i, 4);

    }

    // draw primitive creation vertices 
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer_Pnt);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Position);

    gl.uniform4f(u_FragColor, points_r, points_g, points_b, 1.0);
    gl.drawArrays(gl.POINTS, 0, points.length);    
}

/**
 * Converts 1D or 2D array of Number's 'v' into a 1D Float32Array.
 * @param {Number[] | Number[][]} v
 * @returns {Float32Array}
 */
function flatten(v)
{
    var n = v.length;
    var elemsAreArrays = false;

    if (Array.isArray(v[0])) {
        elemsAreArrays = true;
        n *= v[0].length;
    }

    var floats = new Float32Array(n);

    if (elemsAreArrays) {
        var idx = 0;
        for (var i = 0; i < v.length; ++i) {
            for (var j = 0; j < v[i].length; ++j) {
                floats[idx++] = v[i][j];
            }
        }
    }
    else {
        for (var i = 0; i < v.length; ++i) {
            floats[i] = v[i];
        }
    }

    return floats;
}
